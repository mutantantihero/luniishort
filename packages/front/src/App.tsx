import "./App.css";
import styled from "styled-components";
import { URLShortener } from "./pages/URLShortener";
import { DummyHeader } from "./components/DummyHeader";

const Title = styled.h1`
  font-size: 72px;
  line-height: 84px;
  color: #fff;
  text-align: center;
`;

function App() {
  return (
    <div className="App">
      <DummyHeader />
      <Title>URRL</Title>
      <URLShortener />
    </div>
  );
}

export default App;
