import styled from "styled-components";

export const Flash = styled.div`
  background-color: ${(props: { success?: boolean }) =>
    props.success ? "#D4EDDA" : "#F8D7DA"};
  color: ${(props: { success?: boolean }) =>
    props.success ? "#155724" : "#721C24"};
  height: 103px;
  width: 100%;
  font-size: 36px;
`;
