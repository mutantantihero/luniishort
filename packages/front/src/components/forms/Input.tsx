import styled from "styled-components";

export const Input = styled.input`
  background: #f7f7f7;
  border-radius: 10px;
  width: 100%;
  height: 97px;
`;
