import axios from "axios";
import { SyntheticEvent, useEffect, useState } from "react";
import styled from "styled-components";
import { Button } from "../components/Button";
import { Flash } from "../components/Flash";
import { Input } from "../components/forms/Input";

const Form = styled.form`
  display: flex;
`;

export const URLShortener = () => {
  const [url, setUrl] = useState("");
  const [shortUrl, setShortUrl] = useState("");
  const [error, setError] = useState("");
  const apiUrl = process.env.REACT_APP_API_URL
    ? process.env.REACT_APP_API_URL
    : "http://localhost:3000/api/shorturl/";

  async function postURL() {
    try {
      const { data } = await axios.post(apiUrl, { url });
      setShortUrl(`${process.env.REACT_APP_API_URL}${data.shortURL}`);
    } catch (err: any) {
      setError(err.response.data.error);
    }
  }

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    setShortUrl("");
    setError("");
    postURL();
  };

  useEffect(() => {});
  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Input
          value={url}
          placeholder="URL to shorten"
          onChange={(e) => setUrl(e.target.value)}
        />
        <Button role="submit">Raccourcir</Button>
      </Form>
      <br />

      {shortUrl ? (
        <Flash success={true}>
          <a href={shortUrl}>{shortUrl}</a>
        </Flash>
      ) : null}
      {error ? <Flash>{error}</Flash> : null}
    </div>
  );
};
