import prisma from "../prisma";
import { isUri } from "valid-url";
import { generate } from "short-uuid";
import { URL } from "@prisma/client";
import { URLRepository } from "./URL.repository";

class URLShortenerService {
  async shortenURL(url: string): Promise<URL> {
    if (isUri(url)) {
      const shortURL = generate();
      return URLRepository.create({ url, shortURL });
    } else {
      throw new Error("invalid url");
    }
  }
  getURL(shortURL: string): Promise<URL> {
    return URLRepository.findByShortURL(shortURL);
  }
}

export default new URLShortenerService();
