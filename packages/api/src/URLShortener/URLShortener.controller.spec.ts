import request from "supertest";
import app, { server } from "../app";
import prisma from "../prisma";
describe("URLShortener controller", () => {
  afterAll(() => {
    prisma.$disconnect();
    server.close();
  });
  describe("post /api/shorturl", () => {
    it("should create an URL entity", async () => {
      const payload = { url: "http://google.com" };
      await request(app)
        .post("/api/shorturl")
        .send(payload)
        .expect((res: { body: { url: string } }) => {
          res.body.url === payload.url;
        });
    });
    it("should throw invalid url error", async () => {
      const payload = { url: "oogle.com" };
      await request(app).post("/api/shorturl").send(payload).expect(400);
    });
  });
});
