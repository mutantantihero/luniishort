import URLShortenerService from "./URLShortener.service";
import { URLRepository } from "./URL.repository";
import { URLResponse } from "../DTO/URLResponse.dto";

describe("URLShortenerService", () => {
  it("should shorten and save URL", async () => {
    const url = "https://google.com";
    jest.spyOn(URLRepository, "create").mockImplementation(() => {
      return Promise.resolve({
        id: 1,
        URL: url,
        shortURL: "1234A",
      } as unknown as URLResponse);
    });
    const res = await URLShortenerService.shortenURL(url);
    expect(res.URL).toEqual(url);
  });
  it("should throw when creating with a wrong URL", async () => {
    const url = "//google.com";
    jest.spyOn(URLRepository, "create").mockImplementation(() => {
      return Promise.resolve({
        id: 1,
        URL: url,
        shortURL: "1234A",
      } as unknown as URLResponse);
    });
    expect(URLShortenerService.shortenURL(url)).rejects.toThrow();
  });
});
