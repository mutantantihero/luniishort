import { URLCreateInput } from "../DTO/URLCreateInput.dto";
import { URLResponse } from "../DTO/URLResponse.dto";
import prisma from "../prisma";

export class URLRepository {
  static async create({ url, shortURL }: URLCreateInput): Promise<URLResponse> {
    try {
      const { id, URL } = await prisma.uRL.create({
        data: {
          shortURL,
          URL: url,
        },
      });
      return { id, shortURL, URL };
    } catch (e) {
      throw new Error(e);
    }
  }
  static async findByShortURL(shortURL: string): Promise<URLResponse> {
    try {
      const { id, URL } = await prisma.uRL.findFirst({ where: { shortURL } });
      return { id, shortURL, URL };
    } catch (e) {
      throw new Error(e);
    }
  }
}
