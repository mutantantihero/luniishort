import express, { Router, Request, Response } from "express";
import URLShortenerService from "./URLShortener.service";

const router: Router = express.Router();

router.post("/", async (req: Request, res: Response) => {
  try {
    const URL = await URLShortenerService.shortenURL(req.body.url);
    res.send(URL);
  } catch (e) {
    res.status(400).send({ error: e.toString() });
  }
});

router.get("/:shortURL", async (req: Request, res: Response) => {
  try {
    const URL = await URLShortenerService.getURL(req.params.shortURL);
    URL
      ? res.redirect(URL.URL)
      : res.status(404).send({ error: "URL not found" });
  } catch (e) {
    res.status(500).send(e.toString());
  }
});

export default router;
