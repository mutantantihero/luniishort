require("dotenv").config();
import express from "express";
import compression from "compression";
import bodyParser from "body-parser";
import URLShortenerRouter from "./URLShortener/URLShortener.controller";
import cors from "cors";

const app = express();
const port = process.env.PORT;

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  cors({
    origin: "*",
  })
);

app.use("/api/shorturl", URLShortenerRouter);

export const server = app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});

export default app;
