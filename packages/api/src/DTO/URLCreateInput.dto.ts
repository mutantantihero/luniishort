export type URLCreateInput = {
  url: string;
  shortURL: string;
};
