export type URLResponse = {
  id: number;
  URL: string;
  shortURL: string;
};
