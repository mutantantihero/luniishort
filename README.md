### URRL

URRL is an url shortener.

## To develop localy

First install dependencies: ```npm i```

Then run: ```npm run lerna bootstrap```


## Architecture
All apps are inside the package folder.

- API
  
    Contains the api, it follows some DDD principles.

- Front

    The URRL frontend, this is a react app